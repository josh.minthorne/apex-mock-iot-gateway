package com.levvel.sample.model;

import java.util.List;

public class LockerLocation implements java.io.Serializable
{

   static final long serialVersionUID = 1L;

   private java.lang.String id;

   private List<Locker> lockers;

   public LockerLocation()
   {
   }

   public java.lang.String getId()
   {
      return this.id;
   }

   public void setId(java.lang.String id)
   {
      this.id = id;
   }

   public List<Locker> getLockers()
   {
      return this.lockers;
   }

   public void setLockers(List<Locker> lockers)
   {
      this.lockers = lockers;
   }

   public LockerLocation(java.lang.String id,
         List<Locker> lockers)
   {
      this.id = id;
      this.lockers = lockers;
   }

}