package com.apexsupplychain.clickandcollect;

import java.sql.Timestamp;
import java.text.SimpleDateFormat;

public class SysManagerStart {
	
	private static final String START_TXN_REQ = "StartTxnReq";
	
	private String messageType;
	private String systemMgrToken;
	private String systemMgrSN;
	private String systemMgrClock;
	private String userInput;
	private String inputResource;
	
	private static final SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSZ");
	
	public SysManagerStart() {}
	
	public SysManagerStart(String messageType, String systemMgrToken, String systemMgrSN, String systemMgrClock,
			String userInput, String inputResource) {
		super();
		this.messageType = messageType;
		this.systemMgrToken = systemMgrToken;
		this.systemMgrSN = systemMgrSN;
		this.systemMgrClock = systemMgrClock;
		this.userInput = userInput;
		this.inputResource = inputResource;
	}
	
	public SysManagerStart(String systemMgrToken, String systemMgrSN,
			String userInput, String inputResource) {
		super();
		this.messageType = START_TXN_REQ;
		this.systemMgrToken = systemMgrToken;
		this.systemMgrSN = systemMgrSN;
		this.systemMgrClock = sdf.format(new Timestamp(System.currentTimeMillis())); 
		this.userInput = userInput;
		this.inputResource = inputResource;
	}
	
	
	
	public String getMessageType() {
		return messageType;
	}
	public void setMessageType(String messageType) {
		this.messageType = messageType;
	}
	public String getSystemMgrToken() {
		return systemMgrToken;
	}
	public void setSystemMgrToken(String systemMgrToken) {
		this.systemMgrToken = systemMgrToken;
	}
	public String getSystemMgrSN() {
		return systemMgrSN;
	}
	public void setSystemMgrSN(String systemMgrSN) {
		this.systemMgrSN = systemMgrSN;
	}
	public String getSystemMgrClock() {
		return systemMgrClock;
	}
	public void setSystemMgrClock(String systemMgrClock) {
		this.systemMgrClock = systemMgrClock;
	}
	public String getUserInput() {
		return userInput;
	}
	public void setUserInput(String userInput) {
		this.userInput = userInput;
	}
	public String getInputResource() {
		return inputResource;
	}
	public void setInputResource(String inputResource) {
		this.inputResource = inputResource;
	}
	
	

}
